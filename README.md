# ChatonsInfos

**ChatonsInfos** est un protocole de partage de données sur le collectif, ses membres et leurs services.

L’idée est de faire ça de façon simple et légère via des fichiers de description accessibles par lien web : 1 fichier par chaton et 1 fichier par service. 

Maintenus en autonomie par les chatons volontaires, ces fichiers seront collectés régulièrement et valorisés dans le futur site https://stats.chatons.org/.

 En favorisant un partage d’information sur les chatons et leurs services, les résultats attendus sont :

* une meilleure visibilité des chatons et de leurs services ;
* une mesure de l’activité des services : même incomplète ou partielle, cela donnera idée de l’activité globale.

## LICENCE

Les documents du projet ChatonsInfos sont diffusés sous la licence libre Creative Commons CC-BY-SA (/!\ à valider).

Par défaut, les fichiers de données du protocole ChatonsInfos sont par défaut sous licence ????? (/!\ à compléter).

## AUTEUR

Le collectif CHATONS.

## DOCUMENTATION

Plan de la documentation :
* CONCEPTS.md : les concpets fondamentaux ;
* ONTOLOGIE.md : format et convention des sections génériques ;

## LOGO

Auteur : ?

Licence du logo : Creative Commons CC-BY-SA (/!\ à valider).
